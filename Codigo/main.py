# -*- coding: utf-8 -*-

from Carbon import Snd
import pygame
import sys
from config import *
from os import path
from kid import Player
from map import TiledMap, Camera, pytmx
from menu import show_menu


class KidsGame:
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption('Pasitos Seguros')
        self.clock = pygame.time.Clock()
        self.load_data()


    def load_data(self):
        main_folder = path.dirname('__file__')
        img_folder = path.join(main_folder, "images")
        self.map_folder = path.join(main_folder, "maps")
        self.menu_screen = pygame.image.load(path.join(img_folder, MENU_0))
        self.name_screen = pygame.image.load(path.join(img_folder, MENU_1))
        self.button_img = pygame.image.load(path.join(img_folder, BUTTON))
        self.ok_screen = pygame.image.load(path.join(img_folder, MENU_2))
        self.start_screen = pygame.image.load(path.join(img_folder, START))
        self.alpha_screen = pygame.Surface(self.screen.get_size()).convert_alpha()
        self.alpha_screen.fill((0, 0, 0, 180))
        self.g_over_screen = pygame.image.load(path.join(img_folder, G_OVER))
        self.k_wagen1 = pygame.image.load(path.join(img_folder, WAGEN1))
        self.k_wagen2 = pygame.image.load(path.join(img_folder, WAGEN2))
        self.today_tip = pygame.image.load(path.join(img_folder, T_TIP))


    def new(self):
        self.all_game_sprites = pygame.sprite.Group()
        self.level1_map = TiledMap(path.join(self.map_folder, "level_1.tmx"))
        self.map_surface = self.level1_map.make_map()
        self.map_rect = self.map_surface.get_rect()
        self.player = Player(self)
        # self.all_game_sprites.add(self.player)
        self.player.ini_pos()
        self.camera = Camera(self.level1_map.width, self.level1_map.height)
        self.zebra_list = []
        self.clean_id_list = []
        self.penalty_list = []
        self.count_penalties = []
        self.game_font = pygame.font.Font(None, 35)
        self.high_score = self.game_font.render('High score: 1390', True, CLOUDS, CONCRETE)
        self.game_over = False


    def run(self):
        self.playing = True
        while self.playing:
            self.clock.tick(FPS) / 1000.0
            self.text_score = self.game_font.render("Score: " + str(self.player.score), True, CLOUDS, CONCRETE)
            self.events()
            self.update()
            self.render()


    def bye(self):
        pygame.quit()
        sys.exit()


    def update(self):
        self.all_game_sprites.update()
        self.camera.update(self.player)

        for layer in self.level1_map.tmxdata.visible_layers:
            if isinstance(layer, pytmx.TiledObjectGroup):
                if layer.name == "obstacles":
                    for obj in layer:
                        if pygame.Rect(obj.x, obj.y, obj.width, obj.height).colliderect(self.player.rect) == True:
                            self.player.objects_collision()
                            # print "You hit a wall!"
                            break

        for obj in self.level1_map.tmxdata.objects:
            if pygame.Rect(obj.x, obj.y, obj.width, obj.height).colliderect(self.player.rect) == True:
                self.zebra_list.append(obj.id)
                self.clean_id_list = list(set(self.zebra_list))
                # print self.clean_id_list
                # print len(self.clean_id_list)
                self.player.score = self.seek_groups(self.clean_id_list)
                if obj.name == "meta":
                    self.player.score = self.player.score + 1000
                    # print "Final score: ", self.player.score
                    self.game_over = True
                    break

        for layer in self.level1_map.tmxdata.visible_layers:
            if isinstance(layer, pytmx.TiledObjectGroup):
                if layer.name == "penalties":
                    for obj in layer:
                        if pygame.Rect(obj.x, obj.y, obj.width, obj.height).colliderect(self.player.rect) == True:
                            self.penalty_list.append(obj.id)
                            self.count_penalties = list(set(self.penalty_list))
                            # print self.count_penalties
                            if len(self.count_penalties) == 10:
                                # print "Game Over :("
                                # print "penalty! id: " + str(obj.id)
                                # print "Penalizaciones: " + str(len(self.count_penalties))
                                self.player.accident = True
                                self.game_over = True
                                break

        if self.game_over == True:
            self.playing = False


    def seek_groups(self, clean_list):
        new_list = []

        # GRUPO 1 #################################################
        if 228 in clean_list and 233 in clean_list and 230 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 2 #################################################
        if 231 in clean_list and 234 in clean_list and 226 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 3 #################################################
        if 244 in clean_list and 251 in clean_list and 245 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 4 #################################################
        if 236 in clean_list and 235 in clean_list and 230 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 5 #################################################
        if 226 in clean_list and 237 in clean_list and 232 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 6 #################################################
        if 244 in clean_list and 249 in clean_list and 247 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 7 #################################################
        if 247 in clean_list and 252 in clean_list and 248 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 8 #################################################
        if 245 in clean_list and 250 in clean_list and 248 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 9 #################################################
        if 229 in clean_list and 238 in clean_list and 227 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 10 #################################################
        if 240 in clean_list and 239 in clean_list and 241 in clean_list:
            new_list.append(10)
        else:
            new_list.append(0)

        # GRUPO 11 #################################################
        if 240 in clean_list and 243 in clean_list and 242 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 12 #################################################
        if 241 in clean_list and 258 in clean_list and 256 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 13 #################################################
        if 242 in clean_list and 260 in clean_list and 256 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 14 #################################################
        if 253 in clean_list and 265 in clean_list and 257 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 15 #################################################
        if 261 in clean_list and 267 in clean_list and 262 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 16 #################################################
        if 254 in clean_list and 259 in clean_list and 263 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 17 #################################################
        if 263 in clean_list and 269 in clean_list and 264 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 18 #################################################
        if 255 in clean_list and 268 in clean_list and 254 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 19 #################################################
        if 270 in clean_list and 272 in clean_list and 271 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 20 #################################################
        if 274 in clean_list and 276 in clean_list and 275 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 21 #################################################
        if 277 in clean_list and 278 in clean_list and 279 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 22 #################################################
        if 270 in clean_list and 280 in clean_list and 281 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 23 #################################################
        if 281 in clean_list and 283 in clean_list and 284 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 24 #################################################
        if 284 in clean_list and 285 in clean_list and 271 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 25 #################################################
        if 274 in clean_list and 286 in clean_list and 288 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 26 #################################################
        if 288 in clean_list and 290 in clean_list and 291 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 27 #################################################
        if 275 in clean_list and 292 in clean_list and 291 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 28 #################################################
        if 277 in clean_list and 293 in clean_list and 294 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 29 #################################################
        if 294 in clean_list and 296 in clean_list and 297 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 30 #################################################
        if 279 in clean_list and 298 in clean_list and 297 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 31 #################################################
        if 300 in clean_list and 301 in clean_list and 302 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 32 #################################################
        if 303 in clean_list and 304 in clean_list and 305 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 33 #################################################
        if 303 in clean_list and 306 in clean_list and 307 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 34 #################################################
        if 307 in clean_list and 308 in clean_list and 309 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 35 #################################################
        if 305 in clean_list and 310 in clean_list and 309 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 36 #################################################
        if 311 in clean_list and 312 in clean_list and 313 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 37 #################################################
        if 314 in clean_list and 315 in clean_list and 316 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 38 #################################################
        if 317 in clean_list and 318 in clean_list and 319 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # GRUPO 39 #################################################
        if 320 in clean_list and 321 in clean_list and 322 in clean_list:
            new_list.append(10)

        else:
            new_list.append(0)

        # print "Lista score: ", new_list
        counter = new_list.count(10) * 10
        # print "Score: ", counter

        return counter


    def render(self):
        self.screen.blit(self.map_surface, self.camera.apply_rect(self.map_rect))
        for every_sprite in self.all_game_sprites:
            self.screen.blit(every_sprite.image, self.camera.apply(every_sprite))
        self.screen.blit(self.text_score, [10, 0])
        self.screen.blit(self.high_score, [570, 0])
        pygame.display.flip()


    def events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.bye()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT and self.player.vel_x < 0:
                    self.player.vel_x = 0
                elif event.key == pygame.K_RIGHT and self.player.vel_x > 0:
                    self.player.vel_x = 0
                elif event.key == pygame.K_UP and self.player.vel_y < 0:
                    self.player.vel_y = 0
                elif event.key == pygame.K_DOWN and self.player.vel_y > 0:
                    self.player.vel_y = 0


    def show_start_screen(self):
        self.screen.blit(self.start_screen, [0, 0])
        pygame.display.flip()
        self.waiting_for_user()


    def game_over_screen(self):
        self.screen.blit(self.alpha_screen, [0, 0])
        self.screen.blit(self.g_over_screen, [0, 0])
        final_score = self.game_font.render(str(self.player.score), True, CLOUDS)
        self.screen.blit(final_score, [507, 282])
        pygame.display.flip()
        self.waiting_for_user()


    def accident_screen(self):
        self.screen.fill(MIDNIGHT_BLUE)
        # Animar el gif de la ambulancia
        self.screen.blit(self.k_wagen1, [202, 73])
        self.screen.blit(self.k_wagen2, [202, 73])
        # Preparar algunos consejos viales
        self.screen.blit(self.today_tip, [0, 0])
        warning_text = self.game_font.render("Pulsa cualquier tecla", True, CLOUDS)
        self.screen.blit(warning_text, [257, 500])
        pygame.display.flip()
        self.waiting_for_user()


    def waiting_for_user(self):
        pygame.event.wait()
        waiting = True
        while waiting:
            self.clock.tick(MENU_FPS)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    waiting = False
                    self.bye()
                if event.type == pygame.KEYUP:
                    waiting = False


game = KidsGame()
show_menu(game)
game.show_start_screen()

while True:
    game.new()
    game.run()
    if game.player.accident == False:
        game.game_over_screen()
    if game.player.accident == True:
        game.accident_screen()
