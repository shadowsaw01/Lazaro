import pygame
import config


class CutSprite():
    sprite_sheet = None

    def __init__(self, archive_name):
        self.sprite_sheet = pygame.image.load(archive_name).convert()

    def cut_image(self, x, y, width, height):
        image = pygame.Surface([width, height]).convert()
        image.blit(self.sprite_sheet, (0, 0), (x, y, width, height))
        image.set_colorkey(config.WHITE)
        return image