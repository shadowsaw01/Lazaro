import pygame
from spritesheet import CutSprite


class Player(pygame.sprite.Sprite):
    def __init__(self, game):
        self.groups = game.all_game_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.score = 0
        self.direction = "F"
        self.accident = False

        self.walking_right_frame = []
        self.walking_left_frame = []
        self.walking_back_frame = []
        self.walking_front_frame = []

        self.looking_right_frame = []
        self.looking_left_frame = []
        self.standing_back_frame = []
        self.standing_front_frame = []

        get_sprite_image = CutSprite("images/ch_white.png")

        image = get_sprite_image.cut_image(51, 32, 10, 16)
        self.walking_right_frame.append(image)

        image = get_sprite_image.cut_image(67, 32, 10, 16)
        self.walking_right_frame.append(image)

        image = get_sprite_image.cut_image(83, 32, 10, 16)
        self.walking_right_frame.append(image)

        image = get_sprite_image.cut_image(51, 32, 10, 16)
        self.walking_right_frame.append(image)

        image = get_sprite_image.cut_image(67, 32, 10, 16)
        self.walking_right_frame.append(image)

        image = get_sprite_image.cut_image(83, 32, 10, 16)
        self.walking_right_frame.append(image)

        image = get_sprite_image.cut_image(51, 32, 10, 16)
        self.walking_right_frame.append(image)

        image = get_sprite_image.cut_image(67, 32, 10, 16)
        self.walking_right_frame.append(image)

        image = get_sprite_image.cut_image(83, 32, 10, 16)
        self.walking_right_frame.append(image)

        image = get_sprite_image.cut_image(67, 32, 10, 16)
        self.looking_right_frame.append(image)

        image = get_sprite_image.cut_image(51, 16, 10, 16)
        self.walking_left_frame.append(image)

        image = get_sprite_image.cut_image(67, 16, 10, 16)
        self.walking_left_frame.append(image)

        image = get_sprite_image.cut_image(83, 16, 10, 16)
        self.walking_left_frame.append(image)

        image = get_sprite_image.cut_image(51, 16, 10, 16)
        self.walking_left_frame.append(image)

        image = get_sprite_image.cut_image(67, 16, 10, 16)
        self.walking_left_frame.append(image)

        image = get_sprite_image.cut_image(83, 16, 10, 16)
        self.walking_left_frame.append(image)

        image = get_sprite_image.cut_image(51, 16, 10, 16)
        self.walking_left_frame.append(image)

        image = get_sprite_image.cut_image(67, 16, 10, 16)
        self.walking_left_frame.append(image)

        image = get_sprite_image.cut_image(83, 16, 10, 16)
        self.walking_left_frame.append(image)

        image = get_sprite_image.cut_image(67, 16, 10, 16)
        self.looking_left_frame.append(image)

        image = get_sprite_image.cut_image(51, 48, 10, 16)
        self.walking_back_frame.append(image)

        image = get_sprite_image.cut_image(67, 48, 10, 16)
        self.walking_back_frame.append(image)

        image = get_sprite_image.cut_image(83, 48, 10, 16)
        self.walking_back_frame.append(image)

        image = get_sprite_image.cut_image(51, 48, 10, 16)
        self.walking_back_frame.append(image)

        image = get_sprite_image.cut_image(67, 48, 10, 16)
        self.walking_back_frame.append(image)

        image = get_sprite_image.cut_image(83, 48, 10, 16)
        self.walking_back_frame.append(image)

        image = get_sprite_image.cut_image(51, 48, 10, 16)
        self.walking_back_frame.append(image)

        image = get_sprite_image.cut_image(67, 48, 10, 16)
        self.walking_back_frame.append(image)

        image = get_sprite_image.cut_image(83, 48, 10, 16)
        self.walking_back_frame.append(image)

        image = get_sprite_image.cut_image(67,48, 10, 16)
        self.standing_back_frame.append(image)

        image = get_sprite_image.cut_image(51, 0, 10, 16)
        self.walking_front_frame.append(image)

        image = get_sprite_image.cut_image(67, 0, 10, 16)
        self.walking_front_frame.append(image)

        image = get_sprite_image.cut_image(83, 0, 10, 16)
        self.walking_front_frame.append(image)

        image = get_sprite_image.cut_image(51, 0, 10, 16)
        self.walking_front_frame.append(image)

        image = get_sprite_image.cut_image(67, 0, 10, 16)
        self.walking_front_frame.append(image)

        image = get_sprite_image.cut_image(83, 0, 10, 16)
        self.walking_front_frame.append(image)

        image = get_sprite_image.cut_image(51, 0, 10, 16)
        self.walking_front_frame.append(image)

        image = get_sprite_image.cut_image(67, 0, 10, 16)
        self.walking_front_frame.append(image)

        image = get_sprite_image.cut_image(83, 0, 10, 16)
        self.walking_front_frame.append(image)

        image = get_sprite_image.cut_image(67, 0, 10, 16)
        self.standing_front_frame.append(image)

        self.image = self.standing_front_frame[0]

        self.rect = self.image.get_rect()

    def update(self):
        self.key_pressed()

        self.rect.x += self.vel_x
        pos = self.rect.x

        if self.direction == "R":
            frame = (pos // 20) % len(self.walking_right_frame)
            self.image = self.walking_right_frame[frame]

        if self.direction == "L":
            frame = (pos // 20) % len(self.walking_left_frame)
            self.image = self.walking_left_frame[frame]

        if self.direction == "R" and self.vel_x == 0:
            frame = (pos // 20) % len(self.looking_right_frame)
            self.image = self.looking_right_frame[frame]

        if self.direction == "L" and self.vel_x == 0:
            frame = (pos // 20) % len(self.looking_left_frame)
            self.image = self.looking_left_frame[frame]

        self.rect.y += self.vel_y
        pos = self.rect.y

        if self.direction == "B":
            frame = (pos // 20) % len(self.walking_back_frame)
            self.image = self.walking_back_frame[frame]

        if self.direction == "F":
            frame = (pos // 20) % len(self.walking_front_frame)
            self.image = self.walking_front_frame[frame]

        if self.direction == "B" and self.vel_y == 0:
            frame = (pos // 20) % len(self.standing_back_frame)
            self.image = self.standing_back_frame[frame]

        if self.direction == "F" and self.vel_y == 0:
            frame = (pos // 20) % len(self.standing_front_frame)
            self.image = self.standing_front_frame[frame]

    def key_pressed(self):
        self.vel_x, self.vel_y = 0, 0
        self.p_speed = 2
        keystate = pygame.key.get_pressed()

        if keystate[pygame.K_RIGHT]:
            self.vel_x = 2
            self.direction = "R"
        elif keystate[pygame.K_LEFT]:
            self.vel_x = -2
            self.direction = "L"
        elif keystate[pygame.K_UP]:
            self.vel_y = -2
            self.direction = "B"
        elif keystate[pygame.K_DOWN]:
            self.vel_y = 2
            self.direction = "F"
        if self.vel_x != 0 and self.vel_y != 0:
            self.vel_x /= 2.828
            self.vel_y /= 2.828

    def ini_pos(self):
        self.rect.x += 118
        self.rect.y += 98

    def objects_collision(self):
        if self.direction == 'R':
            self.rect.x -= self.vel_x
            self.vel_x = 0
        if self.direction == 'L':
            self.rect.x -= self.vel_x
            self.vel_x = 0
        if self.direction == 'B':
            self.rect.y -= self.vel_y
            self.vel_y = 0
        if self.direction == 'F':
            self.rect.y -= self.vel_y
            self.vel_y = 0