# -*- coding: utf-8 -*-

# Tamaño de la ventana
WIDTH = 800
HEIGHT = 600

# Colores formato RGB --https://flatuicolors.com/ --

BLACK = (0, 0, 0)

WHITE = (255, 255, 255)

# Purple
AMETHYST = (155, 89, 182)

# Black
WET_ASPHALT = (52, 73, 94)

# Blue
PETER_RIVER = (52, 152, 219)

# Blue
BELIZE_HOLE = (41, 128, 185)

# Dark Purple
WISTERIA = (142, 68, 173)

# Dark blue
MIDNIGHT_BLUE = (44, 62, 80)

# Green
TURQUOISE = (26, 188, 156)

# Green
EMERALD = (46, 204, 113)

# Green
NEPHRITIS = (39, 174, 96)

# Dark green
GREEN_SEA = (22, 160, 133)

# Yellow
SUN_FLOWER = (241, 196, 15)

# Dark yellow
ORANGE = (243, 156, 18)

# Dark orange
PUMPKIN = (211, 84, 0)

# Orange
CARROT = (230, 126, 34)

# Red
ALIZARIN = (231, 76, 60)

# Dark red
POMEGRANATE = (192, 57, 43)

# White
CLOUDS = (236, 240, 241)

# Gray
CONCRETE = (149, 165, 166)

# Gray
SILVER = (189, 195, 199)

# Dark Gray
ASBESTOS = (127, 140, 141)

FPS = 60
MENU_FPS = 30
TILESIZE = 16
MENU_0 = 'menu0.png'
MENU_1 = 'menu1.png'
BUTTON = 'input_button.png'
MENU_2 = 'menu2.png'
START = 'start.png'
G_OVER = 'game_over.png'
WAGEN1 = 'frame1.png'
WAGEN2 = 'frame2.png'
T_TIP = 'todays_tip.png'