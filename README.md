Nueva estructura para Pasitos Seguros según un Tiled Based Game

What we mean or what game designers mean when they're talking about tile based games is that we're using a grid of tiles to layout our game world, the graphics, the map, the movement of the sprites, everything is based on an underlying grid. There's a lot of advantages to this, it helps in organizing the data in your code, it helps in designing the graphics of your levels and you'll see how it goes together.

A lo que nos referimos o a lo que los diseñadores de juegos se refieren al hablar acerca de juegos basados en una cuadrícula es que se usa una rejilla para bosquejar el mundo, las gráficas, el mapa, el movimiento de los sprites, todo está basado en un cuadriculado. Esto tiene demasiadas ventajas, ayuda en la organización de información en el código, ayuda a diseñar los gráficos de tus niveles y verás cómo va todo junto.
